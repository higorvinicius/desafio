FROM python:3.6-alpine
RUN apk add build-base python-dev postgresql-dev
RUN pip install pipenv
ENV PYTHONUNBUFFERED 1
ENV APPDIR /srv/
WORKDIR $APPDIR
ADD Pipfile* $APPDIR
RUN pipenv install
ADD . $APPDIR

CMD pipenv run honcho start
