import xlrd

from .models import Sale, Company, Category


def parse_sales(company_name, file):
    company = Company.objects.create(name=company_name)
    book = xlrd.open_workbook(file_contents=file)

    sheet = book.sheets()[0]
    products = sheet.col_values(0, 1)
    categories = sheet.col_values(1, 1)
    units_solds = sheet.col_values(2, 1)
    cost_prices = sheet.col_values(3, 1)
    total_sales = sheet.col_values(4, 1)

    for product, category, units_sold, cost_price, total_sale in zip(
            products, categories, units_solds,
            cost_prices, total_sales):
        sale = Sale(company=company)
        sale.product = product
        sale.category = Category.objects.get_or_create(name=category)[0]
        sale.units_sold = units_sold
        sale.cost_price = cost_price
        sale.total_sale = total_sale
        sale.save()
    return company
