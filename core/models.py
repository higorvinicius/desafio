from django.db import models


class Company(models.Model):
    name = models.CharField('Name', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'
        ordering = ['name']


class Category(models.Model):
    name = models.CharField('Name', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ['name']


class Sale(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    product = models.CharField('Products', max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    units_sold = models.CharField('Units Sold', max_length=100)
    cost_price = models.CharField('Cost Sale', max_length=100)
    total_sale = models.CharField('Total Sale', max_length=100)

    def __str__(self):
        return self.product

    class Meta:
        verbose_name = 'Sale'
        verbose_name_plural = 'Sales'
        ordering = ['company']
