from django import forms
from django.core.validators import FileExtensionValidator


class UploadFileForm(forms.Form):
    
    company = forms.CharField(label='Empresa')
    email = forms.EmailField(label='E-mail')
    file = forms.FileField(validators=[
        FileExtensionValidator(['xlsx', 'xls'])]
        )
