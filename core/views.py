import os
import uuid

from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.http import HttpResponseBadRequest
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from .models import Company, Sale
from .forms import UploadFileForm
from .jobs import parse_sales_job


class SaletListView(LoginRequiredMixin, ListView):

    template_name = 'core/sales_list_all.html'
    context_object_name = 'sales'
    paginate_by = 50

    def get_queryset(self):
        queryset = Sale.objects.all()
        return queryset


class CompanyList(LoginRequiredMixin, ListView):
    model = Company


class CompanyView(LoginRequiredMixin, DetailView):
    model = Company
    context_object_name = 'company'


class CompanyCreate(LoginRequiredMixin, CreateView):
    model = Company
    fields = ['name']
    success_url = reverse_lazy('new')


class CompanyUpdate(LoginRequiredMixin, UpdateView):
    model = Company
    fields = ['name']
    success_url = reverse_lazy('list')


class CompanyDelete(LoginRequiredMixin, DeleteView):
    model = Company
    success_url = reverse_lazy('list')


def import_data(request):
    success = False

    form = UploadFileForm(request.POST,
                            request.FILES)

    if form.is_valid():
        company_name = form.cleaned_data['company']
        email = form.cleaned_data['email']
        spreadsheet = request.FILES.get('file')
        root_name, ext = os.path.splitext(spreadsheet.name)
        basename = '%s.%s' % (uuid.uuid1(), ext)

        spreadsheet_destiny = os.path.join(
            settings.SPREADSHEET_FILES_PATH,
            basename
        )
        with open(spreadsheet_destiny, 'wb') as f:
            f.write(spreadsheet.read())

        parse_sales_job(company_name, email, basename)

        send_mail(
            'Suas vendas estão sendo processadas',
            """O arquivo de vendas da empres %s foi adicionado
             na fila de processamento""" % company_name,
            'no-reply@vendas.com',
            [ email ]
        )
        messages.info(request, 'Suas vendas estão sendo processadas')
    elif request.method == 'POST':
        messages.error(request, 'Formulário inválido')

    return render(

        request,
        'core/upload_form.html',
        {
            'form': form,
            'success': success,
        })
