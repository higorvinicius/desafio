from django.urls import path

from . import views

urlpatterns = [
    path('list/', views.CompanyList.as_view(), name='list'),
    path('new/', views.import_data, name="new"),
    path('view/<int:pk>', views.CompanyView.as_view(), name='view'),
    path('edit/<int:pk>', views.CompanyUpdate.as_view(), name='edit'),
    path('delete/<int:pk>', views.CompanyDelete.as_view(), name='delete'),
    path('', views.SaletListView.as_view(), name='list_all'),

]
