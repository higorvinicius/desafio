import os

from django.conf import settings
from django.core.mail import send_mail
from django.conf import settings
from django.urls import reverse
from django.core.mail import send_mail

from background_task import background

from .services import parse_sales


@background(queue='sales')
def parse_sales_job(company_name, email, filename):
    fullname = os.path.join(
        settings.SPREADSHEET_FILES_PATH,
        filename
    )

    with open(fullname, 'rb') as f:
        company = parse_sales(company_name, f.read())

    send_mail(
        'Suas vendas foram processadas',
        """ O arquivo de vendas da empresa %s foi processado
        e pode ser acessado em http://0.0.0.0:8000%s""" %
        (company.name, reverse('view', args=[company.id])),
        'no-reply@vendas.com',
        [email]
    )