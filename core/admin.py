from django.contrib import admin
from .models import Sale, Category, Company


admin.site.register(Sale)
admin.site.register(Category)
admin.site.register(Company)
