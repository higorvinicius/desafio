# Desafio Finxi
Sistema web para importação e processamento de vendas.


### Clonar o repositório
```
git clone https://github.com/HigorMonteiro/desafio_finxi.git
cd desafio_finxi/
mkdir spreadsheets

```
### Dependências

1. [Pipenv](https://pipenv.readthedocs.io/en/latest/)
2. [Install docker](https://docs.docker.com/install/)
3. [Install the docker-compose](https://docs.docker.com/compose/install/)


### Vamos roda a aplicação ultilizando o Docker:

```
docker-compose build
docker-compose run web pipenv run python manage.py migrate
docker-compose run web pipenv run python manage.py createsuperuser
docker-compose up
```


### Visualize o teste de envio de email com o [mailcatcher](https://mailcatcher.me/)
Na ```Porta 1080``` você pode visualizar as notificações do informando que o arquivo foi recebido e foi processado.

```
http://localhost:1080/
```